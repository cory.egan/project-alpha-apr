# Task Flow

Here are some screenshots of the working website!
I accessed this by first going to my project directory.
Opening my virtual environment.
Then running ```python manage.py runserver```
This is accessed from my localhost:8000/

**Home Page and My Projects**
http://localhost:8000/projects/
![alt text](images/home_page.png)



**Create a new project**
http://localhost:8000/projects/create/
![alt text](images/create_projects.png)



**My Tasks**
http://localhost:8000/tasks/mine/
![alt text](images/my_tasks.png)



**Create a new task**
http://localhost:8000/tasks/create/
![alt text](images/create_tasks.png)
